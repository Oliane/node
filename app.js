var app = require('./config/server');

var rotaNoticias = require('./app/routes/noticias')(app);

var rotaHome = require('./app/routes/home')(app);

var rotaCuriosidades = require('./app/routes/curiosidades')(app);

var rotaEsporte = require('./app/routes/esporte')(app);

var rotaFormInclusaoNoticia = require('./app/routes/form_inclusao_noticia')(app);

app.listen(3000, function(){
    console.log("Servidor ON");
});